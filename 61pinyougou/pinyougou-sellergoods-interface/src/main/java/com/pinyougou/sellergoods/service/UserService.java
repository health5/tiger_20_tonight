package com.pinyougou.sellergoods.service;
import com.pinyougou.pojo.TbUser;

import com.github.pagehelper.PageInfo;
import com.pinyougou.core.service.CoreService;

import java.util.List;
import java.util.Map;

/**
 * 服务层接口
 * @author Administrator
 *
 */
public interface UserService extends CoreService<TbUser> {
	
	
	
	/**
	 * 返回分页列表
	 * @return
	 */
	 PageInfo<TbUser> findPage(Integer pageNo, Integer pageSize);
	
	

	/**
	 * 分页
	 * @param pageNo 当前页 码
	 * @param pageSize 每页记录数
	 * @return
	 */
	PageInfo<TbUser> findPage(Integer pageNo, Integer pageSize, TbUser User);

    void updateOne(Long id, String flag);

	void updateList(Long[] ids, String flag);

	/**
	 * report 用户统计
	 * @param monthList
	 * @return
	 */
    List<Integer> findUserCountByMonths(List<String> monthList);

	/**
	 * 获取execl数据
	 * @return
	 */
	List<Map<String, Object>> getBusinessReportData();

	/**
	 * 用户统计
	 * @return
	 */
    Map<String,Object> getBusinessReport();
}
