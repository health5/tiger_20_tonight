package com.pinyougou.order.service;

import com.github.pagehelper.PageInfo;
import com.pinyougou.core.service.CoreService;
import com.pinyougou.pojo.OrderCount;
import com.pinyougou.pojo.TbOrderItem;
import entity.OrderCountTerm;


/**
 * 服务层接口
 * @author Administrator
 *
 */
public interface OrderItemService extends CoreService<TbOrderItem> {
	
	
	
	/**
	 * 返回分页列表
	 * @return
	 */
	 PageInfo<TbOrderItem> findPage(Integer pageNo, Integer pageSize);



	/**
	 * 分页
	 * @param pageNo 当前页 码
	 * @param pageSize 每页记录数
	 * @return
	 */
	PageInfo<TbOrderItem> findPage(Integer pageNo, Integer pageSize, TbOrderItem OrderItem);

    /**
     * 订单统计分页
	 * @param pageNo
     * @param pageSize
     * @param orderCountTerm
     * @return
     */
	public PageInfo<OrderCount> orderCount(Integer pageNo, Integer pageSize, OrderCountTerm orderCountTerm);
	
}
