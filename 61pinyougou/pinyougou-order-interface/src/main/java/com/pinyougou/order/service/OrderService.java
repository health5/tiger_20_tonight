package com.pinyougou.order.service;

import com.github.pagehelper.PageInfo;
import com.pinyougou.pojo.TbGoods;
import com.pinyougou.pojo.TbOrder;
import com.pinyougou.pojo.TbOrderItem;
import com.pinyougou.pojo.TbPayLog;

import java.util.List;
import java.util.Map;

/**
 * 描述
 *
 * @author 三国的包子
 * @version 1.0
 * @package com.pinyougou.order.service *
 * @since 1.0
 */
public interface OrderService {
    void add(TbOrder order);

    /**
     * 根据当前的用户的ID 获取 当前用户的支付的日志
     * @param userId
     * @return
     */
    TbPayLog getPayLogByUserId(String userId);

    /**
     *
     * +
     * 更新支付的状态(支付的时间,支付的状态,transaction_id)

     +  删除redis的当前用户的支付日志

     + 更新订单的状态()
     * @param out_trade_no
     * @param transaction_id
     */
    void updateStatus(String out_trade_no, String transaction_id);

    List<TbOrder> findAll();

    PageInfo<TbOrder> findPage(Integer pageNo, Integer pageSize);

    PageInfo<TbOrder> findPage(Integer pageNo, Integer pageSize,TbOrder order);


    Map<String,Object> getTongJiData() throws Exception;

    /**
     * 用户分页查询订单
     * @param pageNo 当前页 码
     * @param pageSize 每页记录数
     * @return
     */

    PageInfo<TbOrderItem> findUserOrder(Integer pageNo, Integer pageSize, TbOrder order);

    /**
     * 商家订单查询
     * @param pageNo
     * @param pageSize
     * @param order
     * @return
     */

    public PageInfo<TbOrder> findPages(Integer pageNo, Integer pageSize, TbOrder order);


}
