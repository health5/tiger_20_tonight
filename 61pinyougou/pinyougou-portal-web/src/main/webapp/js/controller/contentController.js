var app = new Vue({
    el: "#app",
    data: {
        pages: 15,
        pageNo: 1,
        list: [],
        entity: {},
        keywords:'',//搜索的关键字
        contentList:[], //广告的列表数据
        contentList2:[],//今日推荐列表数据
        contentList3:[],//首页黑马广告
        contentList4:[],
        contentList5:[],//品优购快报
        ids: [],
        itemCat1List:[],
        itemCat2List:[],
        itemCat3List:[],
        searchEntity: {}
    },
    methods: {
        //在页面加载的调用 根据分类的ID 获取广告的列表数据赋值一个变量 页面循环遍历即可
        findByCategoryId:function (categoryId) {
            axios.get('/content/findByCategoryId/'+categoryId+'.shtml').then(
                function (response) {//response.data=list<contenet>
                    app.contentList=response.data;
                }
            )
        },

        findByCategoryId2:function (categoryId) {
            axios.get('/content/findByCategoryId/'+categoryId+'.shtml').then(
                function (response) {//response.data=list<contenet>
                    app.contentList2=response.data;
                }
            )
        },
        findByCategoryId3:function (categoryId) {
            axios.get('/content/findByCategoryId/'+categoryId+'.shtml').then(
                function (response) {//response.data=list<contenet>
                    app.contentList3=response.data;
                }
            )
        },
        findByCategoryId4:function (categoryId) {
            axios.get('/content/findByCategoryId/'+categoryId+'.shtml').then(
                function (response) {//response.data=list<contenet>
                    app.contentList4=response.data;
                }
            )
        },

        findByCategoryId5:function (categoryId) {
            axios.get('/content/findByCategoryId/'+categoryId+'.shtml').then(
                function (response) {//response.data=list<contenet>
                    app.contentList5=response.data;
                }
            )
        },
        //页面加载的时候查询一级分类的列表
        findItemCat1List: function (id) {
            axios.get('/itemCat/findByParentId/'+id+'.shtml').then(
                function (response) {
                    app.itemCat1List = response.data;
                }
            )
        },
        findItemCat2List: function (id) {
            axios.get('/itemCat/findByParentId/'+id+'.shtml').then(
                function (response) {
                    app.itemCat2List = response.data;
                }
            )
        },
        findItemCat3List: function (id) {
            axios.get('/itemCat/findByParentId/'+id+'.shtml').then(
                function (response) {
                    app.itemCat3List = response.data;
                }
            )
        },
        //1.获取u关键字的值 跳转到搜索的页面 携带参数
        doSearch:function () {


            //要转码
            window.location.href="http://localhost:9104/search.html?keywords="+encodeURIComponent(this.keywords);

        }
    },
    created: function () {
            this.findByCategoryId(1);
            this.findByCategoryId2(2);
            this.findByCategoryId3(3);
            this.findByCategoryId4(4);
            this.findByCategoryId5(5);
            this.findItemCat1List(0);
    }
});