package com.pinyougou.mapper;

import com.pinyougou.pojo.OrderCount;
import com.pinyougou.pojo.TbOrderItem;
import entity.OrderCountTerm;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface TbOrderItemMapper extends Mapper<TbOrderItem> {
    List<OrderCount> orderCount(OrderCountTerm orderCountTerm);
}