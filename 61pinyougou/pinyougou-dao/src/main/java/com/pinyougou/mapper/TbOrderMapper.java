package com.pinyougou.mapper;

import com.pinyougou.pojo.TbOrder;
import tk.mybatis.mapper.common.Mapper;

public interface TbOrderMapper extends Mapper<TbOrder> {
    Integer findOrderCountByDate(String reportDate);

    Integer findOrderCountWeekDate(String thisWeekMonday);

    Integer findOrderCountAfterDate(String firstDay4ThisMonth);

}