package com.pinyougou.mapper;

import com.pinyougou.pojo.TbUser;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TbUserMapper extends Mapper<TbUser> {
    Integer findUserCountBeforeDate(String date);

    List<Map<String,Object>> getBusinessReportData();

    Integer findUserCountByDate(String reportDate);

    Integer findUserTotalCount();

    Integer findUserCountAfterDate(String thisWeekMonday);
}