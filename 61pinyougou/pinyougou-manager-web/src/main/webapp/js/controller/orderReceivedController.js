var app = new Vue({
    el: '#app',
    data:{
        orderData:{
            todayDate:null,//今日日期

            totalOrder :0,//总订单数

            todayOrderNumber :0,//今日订单数

            thisWeekOrderNumber :0,//本周订单数

            thisMonthOrderNumber :0,//本月订单数

            weixinPay:0,//微信支付

            deferredPayment:0//货到付款

        }
    },
    created() {
        //发送ajax请求获取动态数据
        axios.get("/tongji/getTongJiData.shtml").then(function (response) {
            app.orderData=response.data;
        });
    }
})