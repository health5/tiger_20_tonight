var app = new Vue({
    el: "#app",
    data: {
        pages:15,
        pageNo:1,
        list:[],
        entity:{},
        ids:[],
        searchEntity:{}
    },
    methods: {
        //查询所有订单列表
        findAll:function () {
            console.log(app);
            axios.get('/seckillOrderManager/findAll.shtml').then(function (response) {
                app.list= response.data
            }).catch(function (error) {

            })
        },

        searchList:function (curpage) {
            axios.post('/seckillOrderManager/search.shtml?pageNo='+curpage,this.searchEntity).then(function (response){

                console.log(response.data)
                app.list=response.data.list
                app.pageNo=curpage;
                app.pages=response.data.pages;
            })
        }

    },
    //钩子函数 初始化了事件和
    created: function () {
        // this.findAll();
        this.searchList(1);

    }
})
