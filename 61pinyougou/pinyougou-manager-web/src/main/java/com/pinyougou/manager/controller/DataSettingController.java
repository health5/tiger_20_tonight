package com.pinyougou.manager.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.manager.untils.POIUtils;
import com.pinyougou.pojo.TbBrand;
import com.pinyougou.pojo.TbSpecification;
import com.pinyougou.sellergoods.service.BrandService;
import com.pinyougou.sellergoods.service.SpecificationService;
import entity.ResultInfo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 预约设置
 */
@RestController
@RequestMapping("/dataSetting")
public class DataSettingController {
    @Reference
    private BrandService brandService;
    @Reference
    private SpecificationService specificationService;
    //Excel文件上传，并解析文件内容保存到数据库
    @RequestMapping("/brandUpload")
    public ResultInfo brandUpload(@RequestParam("excelFile")MultipartFile excelFile){
        try {
            //读取Excel文件数据
            List<String[]> list = POIUtils.readExcel(excelFile);
            if(list != null && list.size() > 0){
                List<TbBrand> brandSettingList = new ArrayList<>();
                for (String[] strings : list) {
                    TbBrand tbBrand = new TbBrand(strings[0], strings[1]);
                    brandSettingList.add(tbBrand);
                }
                brandService.addSeries(brandSettingList);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return new ResultInfo(false,"上传失败" );
        }
        return new ResultInfo(true,"上传成功");
    }

    @RequestMapping("/specificationUpload")
    public ResultInfo specificationUpload(@RequestParam("excelFile")MultipartFile excelFile){
        try {
            //读取Excel文件数据
            List<String[]> list = POIUtils.readExcel(excelFile);
            if(list != null && list.size() > 0){
                List<TbSpecification> specificationSettingList = new ArrayList<>();
                for (String[] strings : list) {
                    TbSpecification specification = new TbSpecification(strings[0]);
                    specificationSettingList.add(specification);
                }
                specificationService.addSeries(specificationSettingList);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return new ResultInfo(false,"上传失败" );
        }
        return new ResultInfo(true,"上传成功");
    }
}
