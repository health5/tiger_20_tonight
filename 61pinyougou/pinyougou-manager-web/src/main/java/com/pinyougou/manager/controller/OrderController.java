package com.pinyougou.manager.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.pinyougou.order.service.OrderService;
import com.pinyougou.pojo.TbOrder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 描述
 *
 * @author 三国的包子
 * @version 1.0
 * @package com.pinyougou.cart.controller *
 * @since 1.0
 */
@RestController
@RequestMapping("/orderManager")
public class OrderController {

    @Reference
    private OrderService orderService;

    @RequestMapping("/findAll")
    public List<TbOrder> findAll(){

        return orderService.findAll();
    }

    @RequestMapping("/findPage")
    public PageInfo<TbOrder> findPage(@RequestParam(value = "pageNo",defaultValue = "1",required = true) Integer pageNo,
                                   @RequestParam(value = "pages",defaultValue = "10",required = true) Integer pageSize){
        System.out.println("pageNo========"+pageNo);
        PageInfo<TbOrder> page = orderService.findPage(pageNo, pageSize);

        return page;
    }

    @RequestMapping("/search")
    public PageInfo<TbOrder> findPage(@RequestParam(value = "pageNo", defaultValue = "1", required = true) Integer pageNo,
                                      @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                     @RequestBody TbOrder order
                                      ) {
        System.out.println("searchEntity================="+order);

        PageInfo<TbOrder> page = orderService.findPage(pageNo, pageSize, order);

        System.out.println("page================="+page);

        return page;
    }

}
