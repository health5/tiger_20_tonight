package com.pinyougou.manager.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.order.service.OrderService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/tongji")
public class TongJiController {

    @Reference
    private OrderService orderService;

    @RequestMapping("/getTongJiData")
    public Map getTongJiData(){
        Map<String, Object> result= null;
        try {
            result = orderService.getTongJiData();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
