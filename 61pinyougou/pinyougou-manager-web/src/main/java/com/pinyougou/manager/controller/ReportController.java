package com.pinyougou.manager.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.common.util.DateUtils;
import com.pinyougou.sellergoods.service.UserService;


import entity.ResultInfo;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.elasticsearch.index.mapper.ObjectMapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

/**
 * @Description:
 * @Author: yp
 */
@RestController
@RequestMapping("/report")
public class ReportController {


    @Reference
    private UserService userService;
    /**
     * 获得会员数量,日期报表
     * @return
     */
    @RequestMapping("/getUserReport")
    public ResultInfo getMemberReport(){
        try {
            //1.创建Map
            Map resultMap = new HashMap();
            //2.封装months(过去一年的月份)
            List<String> monthList = new ArrayList<String>();
            Calendar calendar = Calendar.getInstance(); //当前的日期
            calendar.add(Calendar.MONTH,-12); //月份-12  2018-06
            for(int i =0;i<12;i++){
                calendar.add(Calendar.MONTH,1); //2018-07
                String month = DateUtils.parseDate2String(calendar.getTime(), "yyyy-MM");
                monthList.add(month);
            }
            resultMap.put("months",monthList);
            //3.调用service 获得 过去一年每个月份的用户总数量(累加的)
            List<Integer> userCountList  = userService.findUserCountByMonths(monthList);
            //4.封装memberCount resultMap
            resultMap.put("userCount",userCountList);
            return new ResultInfo(true,"成功",resultMap);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultInfo(false,"失败");
        }
    }

    /**
     * 导出用户数据
     *
     * */

    @RequestMapping("/getBusinessReportData")
    public ResultInfo getBusinessReportData (HttpServletRequest request, HttpServletResponse response){
        try {
            //1.调用业务获得运营数据
            // payment_type=1, STATUS=0, update_time=2017-08-24 20:46:10.0, receiver_area_name=金燕龙办公楼,
            // create_time=2017-08-24 20:46:10.0, receiver=李嘉诚, user_id=lijialong, item_id=19, payment=3.00,
            // order_id=3, receiver_mobile=13900112222
            List<Map<String, Object>> resultInfo = userService.getBusinessReportData();
            System.out.println("resultInfo======"+resultInfo);
            //2.根据模版创建工作簿对象
            InputStream is = request.getSession().getServletContext().getResourceAsStream("template/user_template.xlsx");
            XSSFWorkbook workbook = new XSSFWorkbook(is);
            //3.获得工作表

            XSSFSheet sheet = workbook.getSheetAt(0);
            int rowAdd=3;
            XSSFRow row ;
            for (Map<String, Object> map : resultInfo) {
                System.out.println(map);
                String userId =(String) map.get("user_id");
                BigDecimal pay = (BigDecimal) map.get("payment");
                double payment = pay.doubleValue();//
                String paymentType =(String) map.get("payment_type");
                String status =(String) map.get("STATUS");

                Date create =(Date) map.get("create_time");
                String createTime = create.toLocaleString();
                Date update =(Date) map.get("update_time");
                String updateTime = update.toLocaleString();
                Long order =(Long) map.get("order_id");
                String orderId  = order.toString();
                String receiverAreaName=(String) map.get("receiver_area_name");
                String receiverMobile =(String) map.get("receiver_mobile");
                String receiver =(String) map.get("receiver");
                Long itemId =(Long) map.get("item_id");
                Object seller =map.get("seller_id");
                String sellerId="";
                if (seller==null){
                    sellerId = null;
                }else {
                   sellerId = seller.toString();
                }
            //4. 获得行, 列, 把数据进行填充
                row = sheet.getRow(rowAdd++);
                row.getCell(0).setCellValue(userId);
                row.getCell(1).setCellValue(payment);
                row.getCell(2).setCellValue(paymentType);
                row.getCell(3).setCellValue(status);
                row.getCell(4).setCellValue(createTime);
                row.getCell(5).setCellValue(updateTime);
                row.getCell(6).setCellValue(orderId);
                row.getCell(7).setCellValue(receiverAreaName);
                row.getCell(8).setCellValue(receiverMobile);
                row.getCell(9).setCellValue(receiver);
                row.getCell(10).setCellValue(itemId);
                row.getCell(11).setCellValue(sellerId);

            }
            //5, 通过文件流响应
            ServletOutputStream os = response.getOutputStream();
            //设置两个头(告诉浏览器文件的mime类型, 告诉浏览器下载)
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setHeader("Content-Disposition","attachment;filename=user.xlsx");
            workbook.write(os);
            os.flush();
            os.close();
            is.close();
            workbook.close();
            return new ResultInfo(true, "成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultInfo(false, "失败");
        }
    }
    @RequestMapping("/exportBusinessReport")
    public ResultInfo exportBusinessReport(){
        try {
            Map<String, Object> result = userService.getBusinessReport();
            return new ResultInfo(true,"成功",result);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultInfo(false,"失败");
        }
    }
}
