package com.pinyougou.manager.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.pinyougou.pojo.TbOrder;
import com.pinyougou.pojo.TbSeckillOrder;
import com.pinyougou.seckill.service.SeckillOrderService;
import entity.Result;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述
 *
 * @author 三国的包子
 * @version 1.0
 * @package com.pinyougou.seckill.controller *
 * @since 1.0
 */
@RestController
@RequestMapping("/seckillOrderManager")
public class SeckillOrderController {

    @Reference
    private SeckillOrderService orderService;

    /**
     * 下单
     *
     * @param
     * @return
     */
    @RequestMapping("/findPage")
    public PageInfo<TbSeckillOrder> findPage(@RequestParam(value = "pageNo",defaultValue = "1",required = true) Integer pageNo,
                                      @RequestParam(value = "pages",defaultValue = "10",required = true) Integer pageSize){



        PageInfo<TbSeckillOrder> page= orderService.findPage(pageNo,pageSize);
        return page;
    }

    @RequestMapping("/search")
    public PageInfo<TbSeckillOrder> findPage(@RequestParam(value = "pageNo", defaultValue = "1", required = true) Integer pageNo,
                                      @RequestParam(value = "pageSize", defaultValue = "10", required = true) Integer pageSize,
                                      @RequestBody TbSeckillOrder seckillOrder) {

        PageInfo<TbSeckillOrder> page = orderService.findPage(pageNo, pageSize, seckillOrder);
        return page;
    }

}
