package com.pinyougou.user.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pinyougou.mapper.TbOrderItemMapper;
import com.pinyougou.pojo.TbOrderItem;
import com.pinyougou.user.service.OrderService;
import entity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo; 									  
import org.apache.commons.lang3.StringUtils;
import com.pinyougou.core.service.CoreServiceImpl;

import tk.mybatis.mapper.entity.Example;

import com.pinyougou.mapper.TbOrderMapper;
import com.pinyougou.pojo.TbOrder;  





/**
 * 服务实现层
 * @author Administrator
 *
 */
@Service
public class OrderServiceImpl extends CoreServiceImpl<TbOrder>  implements OrderService {


	private TbOrderMapper orderMapper;

	@Autowired
	private TbOrderItemMapper orderItemMapper;



	@Autowired
	public OrderServiceImpl(TbOrderMapper orderMapper) {
		super(orderMapper, TbOrder.class);
		this.orderMapper=orderMapper;

	}

	
	

	
	@Override
    public PageInfo<TbOrder> findPage(Integer pageNo, Integer pageSize) {
        PageHelper.startPage(pageNo,pageSize);
        List<TbOrder> all = orderMapper.selectAll();
        PageInfo<TbOrder> info = new PageInfo<TbOrder>(all);

        //序列化再反序列化
        String s = JSON.toJSONString(info);
        PageInfo<TbOrder> pageInfo = JSON.parseObject(s, PageInfo.class);
        return pageInfo;
    }

	
	

	 @Override
    public Map<String, Object> findPage(Integer pageNo, Integer pageSize, TbOrder order) {
        PageHelper.startPage(pageNo,pageSize);

        Example example = new Example(TbOrder.class);
        Example.Criteria criteria = example.createCriteria();

        if(order!=null){			
						if(StringUtils.isNotBlank(order.getPaymentType())){
				criteria.andLike("paymentType","%"+order.getPaymentType()+"%");
				//criteria.andPaymentTypeLike("%"+order.getPaymentType()+"%");
			}
			if(StringUtils.isNotBlank(order.getPostFee())){
				criteria.andLike("postFee","%"+order.getPostFee()+"%");
				//criteria.andPostFeeLike("%"+order.getPostFee()+"%");
			}
			if(StringUtils.isNotBlank(order.getStatus())){
				criteria.andLike("status","%"+order.getStatus()+"%");
				//criteria.andStatusLike("%"+order.getStatus()+"%");
			}
			if(StringUtils.isNotBlank(order.getShippingName())){
				criteria.andLike("shippingName","%"+order.getShippingName()+"%");
				//criteria.andShippingNameLike("%"+order.getShippingName()+"%");
			}
			if(StringUtils.isNotBlank(order.getShippingCode())){
				criteria.andLike("shippingCode","%"+order.getShippingCode()+"%");
				//criteria.andShippingCodeLike("%"+order.getShippingCode()+"%");
			}
			if(StringUtils.isNotBlank(order.getUserId())){
				criteria.andLike("userId","%"+order.getUserId()+"%");
				//criteria.andUserIdLike("%"+order.getUserId()+"%");
			}
			if(StringUtils.isNotBlank(order.getBuyerMessage())){
				criteria.andLike("buyerMessage","%"+order.getBuyerMessage()+"%");
				//criteria.andBuyerMessageLike("%"+order.getBuyerMessage()+"%");
			}
			if(StringUtils.isNotBlank(order.getBuyerNick())){
				criteria.andLike("buyerNick","%"+order.getBuyerNick()+"%");
				//criteria.andBuyerNickLike("%"+order.getBuyerNick()+"%");
			}
			if(StringUtils.isNotBlank(order.getBuyerRate())){
				criteria.andLike("buyerRate","%"+order.getBuyerRate()+"%");
				//criteria.andBuyerRateLike("%"+order.getBuyerRate()+"%");
			}
			if(StringUtils.isNotBlank(order.getReceiverAreaName())){
				criteria.andLike("receiverAreaName","%"+order.getReceiverAreaName()+"%");
				//criteria.andReceiverAreaNameLike("%"+order.getReceiverAreaName()+"%");
			}
			if(StringUtils.isNotBlank(order.getReceiverMobile())){
				criteria.andLike("receiverMobile","%"+order.getReceiverMobile()+"%");
				//criteria.andReceiverMobileLike("%"+order.getReceiverMobile()+"%");
			}
			if(StringUtils.isNotBlank(order.getReceiverZipCode())){
				criteria.andLike("receiverZipCode","%"+order.getReceiverZipCode()+"%");
				//criteria.andReceiverZipCodeLike("%"+order.getReceiverZipCode()+"%");
			}
			if(StringUtils.isNotBlank(order.getReceiver())){
				criteria.andLike("receiver","%"+order.getReceiver()+"%");
				//criteria.andReceiverLike("%"+order.getReceiver()+"%");
			}
			if(StringUtils.isNotBlank(order.getInvoiceType())){
				criteria.andLike("invoiceType","%"+order.getInvoiceType()+"%");
				//criteria.andInvoiceTypeLike("%"+order.getInvoiceType()+"%");
			}
			if(StringUtils.isNotBlank(order.getSourceType())){
				criteria.andLike("sourceType","%"+order.getSourceType()+"%");
				//criteria.andSourceTypeLike("%"+order.getSourceType()+"%");
			}
			if(StringUtils.isNotBlank(order.getSellerId())){
				criteria.andLike("sellerId","%"+order.getSellerId()+"%");
				//criteria.andSellerIdLike("%"+order.getSellerId()+"%");
			}
	
		}
        List<TbOrder> tbOrders = orderMapper.selectByExample(example);
		 List<Order> orderList = new ArrayList<Order>();

		 if (tbOrders != null || tbOrders.size() > 0){
			 for (TbOrder tborder : tbOrders) {
				 Order order2 = new Order();
				 order2.setTbOrder(tborder);

				 Long orderId = tborder.getOrderId();
				 TbOrderItem tbOrderItem = new TbOrderItem();
				 tbOrderItem.setOrderId(orderId);
				 List<TbOrderItem> tbOrderItems = orderItemMapper.select(tbOrderItem);

				 order2.setOrderItems(tbOrderItems);
				 orderList.add(order2);
			 }

		 }
		 //返回OrderList和pages即可, 思考pages如何获取, pages为order订单的数量, 有多少页, 根据counts/pageSize获得,
		 //查询pageSize
		 List<TbOrder> tbOrders2 = orderMapper.selectByExample(example);
		 int count = tbOrders2.size();
		 int pages = count/pageSize+1;

		 Map<String, Object> pageInfo = new HashMap<String,Object>();
		 pageInfo.put("pages",pages);
		 pageInfo.put("list",orderList);

		 return pageInfo;

		 //PageInfo<Order> info = new PageInfo<Order>(orderList);

		 //返回两个数据: pages,和orderList

       /* //序列化再反序列化
        String s = JSON.toJSONString(info);
        PageInfo<Order> pageInfo = JSON.parseObject(s, PageInfo.class);

        return pageInfo;*/
    }


	@Override
	public PageInfo<Order> findOrderByUseIdPage(Integer pageNo, Integer pageSize,String userId) {
		PageHelper.startPage(pageNo,pageSize);
		List<Order> orderList = new ArrayList<Order>();

		TbOrder tbOrder = new TbOrder();
		tbOrder.setUserId(userId);

		List<TbOrder> tbOrders = orderMapper.select(tbOrder);
		if (tbOrders != null || tbOrders.size() > 0){
			for (TbOrder tborder : tbOrders) {
				Order order = new Order();
				order.setTbOrder(tborder);

				Long orderId = tborder.getOrderId();
				TbOrderItem tbOrderItem = new TbOrderItem();
				tbOrderItem.setOrderId(orderId);
				List<TbOrderItem> tbOrderItems = orderItemMapper.select(tbOrderItem);

				order.setOrderItems(tbOrderItems);
				orderList.add(order);
			}

		}
		PageInfo<Order> ordersPageInfo = new PageInfo<>(orderList);
		String s = JSON.toJSONString(ordersPageInfo);
		PageInfo<Order> pageInfo = JSON.parseObject(s, PageInfo.class);
		return pageInfo;


	}

	@Override
	public List<Order> findOrderByUseId(String userId) {
		List<Order> orderList = new ArrayList<>();

		TbOrder tbOrder = new TbOrder();
		tbOrder.setUserId(userId);

		List<TbOrder> tbOrders = orderMapper.select(tbOrder);

		if (tbOrders != null || tbOrders.size() > 0){
			for (TbOrder tborder : tbOrders) {
				Order order = new Order();
				order.setTbOrder(tborder);

				Long orderId = tborder.getOrderId();
				TbOrderItem tbOrderItem = new TbOrderItem();
				tbOrderItem.setOrderId(orderId);
				List<TbOrderItem> tbOrderItems = orderItemMapper.select(tbOrderItem);

				order.setOrderItems(tbOrderItems);
				orderList.add(order);
			}

		}
		return orderList;
	}



    /*public List<Order> findOrderByUseId(String userId){

		List<Order> orderList = new ArrayList<>();

		TbOrder tbOrder = new TbOrder();
		tbOrder.setUserId(userId);

		List<TbOrder> tbOrders = orderMapper.select(tbOrder);

		if (tbOrders != null || tbOrders.size() > 0){
			for (TbOrder tborder : tbOrders) {
				Order order = new Order();
				order.setTbOrder(tborder);

				Long orderId = tborder.getOrderId();
				TbOrderItem tbOrderItem = new TbOrderItem();
				tbOrderItem.setOrderId(orderId);
				List<TbOrderItem> tbOrderItems = orderItemMapper.select(tbOrderItem);

				order.setOrderItems(tbOrderItems);
				orderList.add(order);
			}

		}
		return orderList;
	}*/
	
}
