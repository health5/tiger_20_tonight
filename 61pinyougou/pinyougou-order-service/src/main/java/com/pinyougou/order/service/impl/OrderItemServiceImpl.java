package com.pinyougou.order.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pinyougou.core.service.CoreServiceImpl;
import com.pinyougou.mapper.TbOrderItemMapper;
import com.pinyougou.order.service.OrderItemService;
import com.pinyougou.pojo.OrderCount;
import com.pinyougou.pojo.TbOrderItem;
import entity.OrderCountTerm;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class OrderItemServiceImpl extends CoreServiceImpl<TbOrderItem> implements OrderItemService {
    private TbOrderItemMapper orderItemMapper;

    @Autowired
    public OrderItemServiceImpl(TbOrderItemMapper orderItemMapper) {
        super(orderItemMapper, TbOrderItem.class);
        this.orderItemMapper=orderItemMapper;
    }

    @Override
    public PageInfo<TbOrderItem> findPage(Integer pageNo, Integer pageSize) {
        return null;
    }

    @Override
    public PageInfo<TbOrderItem> findPage(Integer pageNo, Integer pageSize, TbOrderItem OrderItem) {
        return null;
    }

    public PageInfo<OrderCount> orderCount(Integer pageNo, Integer pageSize, OrderCountTerm orderCountTerm){
        PageHelper.startPage(pageNo, pageSize);
        List<OrderCount> all = orderItemMapper.orderCount(orderCountTerm);
        PageInfo<OrderCount> info = new PageInfo<OrderCount>(all);
        //序列化再反序列化
        String s = JSON.toJSONString(info);
        PageInfo<OrderCount> pageInfo = JSON.parseObject(s, PageInfo.class);
        return pageInfo;
    }
}
