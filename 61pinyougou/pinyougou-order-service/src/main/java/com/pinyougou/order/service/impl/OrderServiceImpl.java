package com.pinyougou.order.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pinyougou.common.util.DateUtils;
import com.pinyougou.common.util.IdWorker;
import com.pinyougou.mapper.TbOrderItemMapper;
import com.pinyougou.mapper.TbOrderMapper;
import com.pinyougou.mapper.TbPayLogMapper;
import com.pinyougou.mapper.TbSeckillOrderMapper;
import com.pinyougou.order.service.OrderService;
import com.pinyougou.pojo.*;
import entity.Cart;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.TestExecutionListeners;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 描述
 *
 * @author 三国的包子
 * @version 1.0
 * @package com.pinyougou.order.service.impl *
 * @since 1.0
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private TbOrderMapper orderMapper;

    @Autowired
    private TbSeckillOrderMapper seckillOrderMapper;

    @Autowired
    private TbOrderItemMapper orderItemMapper;

    @Autowired
    private TbPayLogMapper payLogMapper;

    @Autowired
    private IdWorker idWorker;

    /**
     * 1. 订单拆单 一个商家就是一个订单
     * 2. id生成
     *
     * @param order
     */
    @Override
    public void add(TbOrder order) {

        //1.从redis中获取到购物车列表 (List<Cart>) 一个CART就是一个商家


        List<Cart> cartList = (List<Cart>) redisTemplate.boundHashOps("CART_REDIS_PREFIX").get(order.getUserId());

        double totalFee = 0;

        List<String> orderList = new ArrayList<>();

        for (Cart cart : cartList) {//
            //2.循环遍历 创建订单表 要拆单
            TbOrder ordernew = new TbOrder();
            long orderId = idWorker.nextId();
            orderList.add(orderId + "");//
            ordernew.setOrderId(orderId);//不能自增需要生成
            //ordernew.setPayment();//计算 //todo
            ordernew.setPaymentType(order.getPaymentType());
            ordernew.setPostFee("0");//包邮
            ordernew.setStatus("1");//状态：1、未付款，2、已付款，3、未发货，4、已发货，5、交易成功，6、交易关
            ordernew.setCreateTime(new Date());
            ordernew.setUpdateTime(ordernew.getCreateTime());
            ordernew.setUserId(order.getUserId());
            ordernew.setReceiverAreaName(order.getReceiverAreaName());//收货地址
            ordernew.setReceiverMobile(order.getReceiverMobile());//手机号
            ordernew.setReceiverZipCode("518000");//邮编
            ordernew.setReceiver(order.getReceiver());//收货人
            ordernew.setSourceType("2");//PC
            ordernew.setSellerId(cart.getSellerId());//商家的ID


            //用户在每一个商家买的商品的明细列表
            double totalMoney = 0;
            for (TbOrderItem orderItem : cart.getOrderItemList()) {
                //3.创建订单选项表
                //3.1 生成选项的iD
                long orderItemId = idWorker.nextId();

                orderItem.setId(orderItemId);

                //3.2 订单的ID
                orderItem.setOrderId(orderId);


                totalMoney += orderItem.getTotalFee().doubleValue();

                orderItemMapper.insert(orderItem);
            }

            totalFee += totalMoney;//所有的商家的总金额  元

            ordernew.setPayment(new BigDecimal(totalMoney));//应付金额

            orderMapper.insert(ordernew);

        }

        //创建支付日志 记录
        TbPayLog payLog = new TbPayLog();
        payLog.setOutTradeNo(idWorker.nextId() + "");
        payLog.setCreateTime(new Date());
        double v = totalFee * 100;
        long fen = (long) v;
        payLog.setTotalFee(fen);
        payLog.setUserId(order.getUserId());

        payLog.setTradeState("0");//未支付
        payLog.setOrderList(orderList.toString().replace("[", "").replace("]", ""));//    [1,2,3]
        payLog.setPayType("1");//微信支付
        payLogMapper.insert(payLog);

        //添加到redis中

        redisTemplate.boundHashOps("payLog").put(order.getUserId(), payLog);


        //4.清除掉购物车的数据

        redisTemplate.boundHashOps("CART_REDIS_PREFIX").delete(order.getUserId());//删除登录的用户的购物车


    }

    @Override
    public TbPayLog getPayLogByUserId(String userId) {
        return (TbPayLog) redisTemplate.boundHashOps("payLog").get(userId);
    }

    @Override
    public void updateStatus(String out_trade_no, String transaction_id) {
        //1.先获取到支付日志记录数据
        TbPayLog payLog = payLogMapper.selectByPrimaryKey(out_trade_no);

        //2.更新 状态
        payLog.setPayTime(new Date());//支付的世界
        payLog.setTransactionId(transaction_id);
        payLog.setTradeState("1");//已支付

        payLogMapper.updateByPrimaryKey(payLog);


        //3.获取到该支付日志对应的商品的订单id
        String orderList = payLog.getOrderList();//  37,38

        String[] split = orderList.split(",");

        for (String orderId : split) {
            TbOrder tbOrder = orderMapper.selectByPrimaryKey(Long.valueOf(orderId));
            //4.对应订单状态进行更新

            tbOrder.setStatus("2");tbOrder.setPaymentTime(new Date());
            tbOrder.setUpdateTime(tbOrder.getPaymentTime());

            orderMapper.updateByPrimaryKey(tbOrder);

        }
        //5.删除掉用户的redis中日志记录
        redisTemplate.boundHashOps("payLog").delete(payLog.getUserId());

    }

    @Override
    public List<TbOrder> findAll() {

        return orderMapper.selectAll();
    }

//   @Override
    public PageInfo<TbOrder> findPage(Integer pageNo, Integer pageSize) {
        Page page = PageHelper.startPage(pageNo, pageSize);
        List<TbOrder> orderList = orderMapper.selectAll();

        for (TbOrder order : orderList) {
            if ("1".equals(order.getPaymentType())){
                order.setPaymentType("微信付款");
            }
            if ("2".equals(order.getPaymentType())) {
                order.setPaymentType("货到付款");
            }

            if ("0".equals(order.getStatus())){
                order.setStatus("未支付");
            }
            if ("1".equals(order.getStatus())){
                order.setStatus("已支付");
            }
            if ("2".equals(order.getStatus())){
                order.setStatus("已超时");
            }
        }
        PageInfo<TbOrder> pageInfo = new PageInfo<TbOrder>(orderList);

        String s = JSON.toJSONString(pageInfo);
        PageInfo pageInfo1 = JSON.parseObject(s, PageInfo.class);
        return pageInfo1;
    }

//    @Override
    public PageInfo<TbOrder> findPage(Integer pageNo, Integer pageSize, TbOrder order) {
        PageInfo<TbOrder> pageInfo=null;
        PageHelper.startPage(pageNo, pageSize);

        Example example = new Example(TbOrder.class);
        Example.Criteria criteria = example.createCriteria();

        if (order != null) {
            criteria.andEqualTo("orderId",order.getOrderId());
        }
        List<TbOrder> orderList = orderMapper.selectByExample(example);

        for (TbOrder order1 : orderList) {
            if ("1".equals(order1.getPaymentType())){
                order1.setPaymentType("微信付款");
            }
            if ("2".equals(order1.getPaymentType())) {
                order1.setPaymentType("货到付款");
            }

            if ("0".equals(order1.getStatus())){
                order1.setStatus("未支付");
            }
            if ("1".equals(order1.getStatus())){
                order1.setStatus("已支付");
            }
            if ("2".equals(order1.getStatus())){
                order1.setStatus("已超时");
            }
        }

        PageInfo<TbOrder> info = new PageInfo<TbOrder>(orderList);
        //序列化再反序列化
        String s = JSON.toJSONString(info);
        pageInfo = JSON.parseObject(s, PageInfo.class);
        return pageInfo;

    }


    @Override
    public Map<String, Object> getTongJiData() throws Exception {
      //获得当前日期
//        String today = DateUtils.parseDate2String(DateUtils.getToday());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = Calendar.getInstance().getTime();
        String todayBegin = format.format(date);//今日开始  2014-05-22

        Calendar cal=Calendar.getInstance();
        cal.add(Calendar.DATE,1);
        Date date1 = cal.getTime();
        String todayEnd = format.format(date1);//今日结束 2014-05-23 00:00:00
//        Date dateBegin = DateUtils.parseString2Date(todayBegin);//今日开始日期
//        Date dateEnd = DateUtils.parseString2Date(todayEnd);//今日结束日期

        //获得本周一的日期
        String thisWeekMonday = DateUtils.parseDate2String(DateUtils.getThisWeekMonday());
        //获得本月第一天的日期
        String firstDay4ThisMonth =
                DateUtils.parseDate2String(DateUtils.getFirstDay4ThisMonth());

//        String thisWeekMondayBegin=thisWeekMonday;
//        Date MondayBegin = DateUtils.parseString2Date(thisWeekMondayBegin);//本周开始日期
//        String firstDay4ThisMonthBegin=firstDay4ThisMonth;
//        Date monthBegin = DateUtils.parseString2Date(firstDay4ThisMonthBegin);//本月开始日期




        Example example = new Example(TbOrder.class);

        Example.Criteria criteria0 = example.createCriteria();
        criteria0.andBetween("createTime",todayBegin,todayEnd);
        List<TbOrder> orderList = orderMapper.selectByExample(example);//日订单
        int ordersize = orderList.size();//日订单数量


        Example.Criteria criteria1 = example.createCriteria();
        criteria1.andBetween("createTime",thisWeekMonday,todayEnd);
        List<TbOrder> weekOrderList = orderMapper.selectByExample(example);//周订单
        int weekOrdersize = weekOrderList.size();//周订单数量


        Example.Criteria criteria2 = example.createCriteria();
//        criteria2.andBetween("createTime",monthBegin,dateEnd);
        criteria2.andGreaterThan("createTime",firstDay4ThisMonth);
        List<TbOrder> monthOrderList = orderMapper.selectByExample(example);//月订单
        int monthOrdersize = monthOrderList.size();//月订单数量

        List<TbOrder> orderTotal = orderMapper.selectAll();
        int orderTotalSize = orderTotal.size();//订单总数

        TbOrder order = new TbOrder();
        order.setPaymentType("1");
        List<TbOrder> orders = orderMapper.select(order);//微信支付
        int size = orders.size();

        TbOrder order1 = new TbOrder();
        order1.setPaymentType("2");
        List<TbOrder> orders2 = orderMapper.select(order1);//货到付款
        int size2 = orders2.size();




        Example example1 = new Example(TbSeckillOrder.class);

        Example.Criteria criteria3 = example1.createCriteria();
        criteria3.andBetween("createTime",todayBegin,todayEnd);
        List<TbSeckillOrder> seckillOrderList = seckillOrderMapper.selectByExample(example1);//日秒杀订单
        int seckillOrdersize = seckillOrderList.size();//日秒杀订单数量


        Example.Criteria criteria4 = example1.createCriteria();
        criteria4.andBetween("createTime",thisWeekMonday,todayEnd);
        List<TbSeckillOrder> weekSeckillOrderList = seckillOrderMapper.selectByExample(example1);//周秒杀订单
        int weekSeckillOrdersize = weekSeckillOrderList.size();//周秒杀订单数量


        Example.Criteria criteria5 = example1.createCriteria();
        criteria5.andBetween("createTime",firstDay4ThisMonth,todayEnd);
        List<TbSeckillOrder> monthSeckillOrderList = seckillOrderMapper.selectByExample(example1);//月秒杀订单
        int monthSeckillOrdersize = monthSeckillOrderList.size();//月秒杀订单数量

        List<TbSeckillOrder> seckillOrderTotal = seckillOrderMapper.selectAll();
        int seckillOrderTotalSize = orderTotal.size();//秒杀订单总数


        int todaySize=ordersize+seckillOrdersize;//日总订单数
        int weekSize=weekOrdersize+weekSeckillOrdersize;//周总订单数
        int monthSize=monthOrdersize+monthSeckillOrdersize;//月总订单数
        int orderSize=orderTotalSize+seckillOrderTotalSize;//月总订单数

        Map<String, Object> result = new HashMap<>();
        result.put("todayDate",todayBegin);
        result.put("todayOrderNumber",todaySize);
        result.put("thisWeekOrderNumber",weekSize);
        result.put("thisMonthOrderNumber",monthSize);
        result.put("totalOrder",orderSize);
        result.put("weixinPay",size);
        result.put("deferredPayment",size2);

        return result;
    }

    public PageInfo<TbOrderItem> findUserOrder(Integer pageNo, Integer pageSize, TbOrder order){
        PageHelper.startPage(pageNo, pageSize);

        Example example = new Example(TbOrderItem.class);
        Example.Criteria criteria = example.createCriteria();

        if (order!= null) {
            criteria.andEqualTo("userId",order.getUserId());
        }
        List<TbOrderItem> orderItems = orderItemMapper.selectByExample(example);
        PageInfo<TbOrderItem> info = new PageInfo<TbOrderItem>(orderItems);
        //序列化再反序列化
        String s = JSON.toJSONString(info);
        PageInfo<TbOrderItem> pageInfo = JSON.parseObject(s, PageInfo.class);
        return pageInfo;

    }

    public PageInfo<TbOrder> findPages(Integer pageNo, Integer pageSize, TbOrder order) {
        PageHelper.startPage(pageNo, pageSize);

        Example example = new Example(TbOrder.class);
        Example.Criteria criteria = example.createCriteria();

        if (order!= null) {
            if (order.getOrderId()!=null) {
                criteria.andEqualTo("orderId", order.getOrderId() );
                //criteria.andNameLike("%"+brand.getName()+"%");
            }
            if (StringUtils.isNotBlank(order.getStatus())&&!"0".equals(order.getStatus())){
                criteria.andEqualTo("status",order.getStatus());
            }
            if (StringUtils.isNotBlank(order.getSellerId())){
                criteria.andEqualTo("sellerId",order.getSellerId());
            }

        }
        List<TbOrder> all = orderMapper.selectByExample(example);
        PageInfo<TbOrder> info = new PageInfo<TbOrder>(all);
        //序列化再反序列化
        String s = JSON.toJSONString(info);
        PageInfo<TbOrder> pageInfo = JSON.parseObject(s, PageInfo.class);
        return pageInfo;
    }



}
