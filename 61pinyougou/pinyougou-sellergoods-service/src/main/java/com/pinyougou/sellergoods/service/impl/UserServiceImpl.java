package com.pinyougou.sellergoods.service.impl;

import java.util.*;

import com.pinyougou.common.util.DateUtils;
import com.pinyougou.mapper.TbOrderMapper;
import com.pinyougou.pojo.TbOrder;
import org.apache.poi.ss.usermodel.DateUtil;
import org.elasticsearch.search.DocValueFormat;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import com.pinyougou.core.service.CoreServiceImpl;

import tk.mybatis.mapper.entity.Example;

import com.pinyougou.mapper.TbUserMapper;
import com.pinyougou.pojo.TbUser;

import com.pinyougou.sellergoods.service.UserService;


/**
 * 服务实现层
 *
 * @author Administrator
 */
@Service
public class UserServiceImpl extends CoreServiceImpl<TbUser> implements UserService {

    @Autowired
    private TbOrderMapper tbOrderMapper;
    private TbUserMapper userMapper;

    @Autowired
    public UserServiceImpl(TbUserMapper userMapper) {
        super(userMapper, TbUser.class);
        this.userMapper = userMapper;
    }


    @Override
    public PageInfo<TbUser> findPage(Integer pageNo, Integer pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<TbUser> all = userMapper.selectAll();
        PageInfo<TbUser> info = new PageInfo<TbUser>(all);

        //序列化再反序列化
        String s = JSON.toJSONString(info);
        PageInfo<TbUser> pageInfo = JSON.parseObject(s, PageInfo.class);
        return pageInfo;
    }


    @Override
    public PageInfo<TbUser> findPage(Integer pageNo, Integer pageSize, TbUser user) {
        PageHelper.startPage(pageNo, pageSize);

        Example example = new Example(TbUser.class);
        Example.Criteria criteria = example.createCriteria();

        if (user != null) {
            if (StringUtils.isNotBlank(user.getUsername())) {
                criteria.andLike("username", "%" + user.getUsername() + "%");
                //criteria.andUsernameLike("%"+user.getUsername()+"%");
            }
            if (StringUtils.isNotBlank(user.getPassword())) {
                criteria.andLike("password", "%" + user.getPassword() + "%");
                //criteria.andPasswordLike("%"+user.getPassword()+"%");
            }
            if (StringUtils.isNotBlank(user.getPhone())) {
                criteria.andLike("phone", "%" + user.getPhone() + "%");
                //criteria.andPhoneLike("%"+user.getPhone()+"%");
            }
            if (StringUtils.isNotBlank(user.getEmail())) {
                criteria.andLike("email", "%" + user.getEmail() + "%");
                //criteria.andEmailLike("%"+user.getEmail()+"%");
            }
            if (StringUtils.isNotBlank(user.getSourceType())) {
                criteria.andLike("sourceType", "%" + user.getSourceType() + "%");
                //criteria.andSourceTypeLike("%"+user.getSourceType()+"%");
            }
            if (StringUtils.isNotBlank(user.getNickName())) {
                criteria.andLike("nickName", "%" + user.getNickName() + "%");
                //criteria.andNickNameLike("%"+user.getNickName()+"%");
            }
            if (StringUtils.isNotBlank(user.getName())) {
                criteria.andLike("name", "%" + user.getName() + "%");
                //criteria.andNameLike("%"+user.getName()+"%");
            }
            if (StringUtils.isNotBlank(user.getStatus())) {
                criteria.andLike("status", "%" + user.getStatus() + "%");
                //criteria.andStatusLike("%"+user.getStatus()+"%");
            }
            if (StringUtils.isNotBlank(user.getHeadPic())) {
                criteria.andLike("headPic", "%" + user.getHeadPic() + "%");
                //criteria.andHeadPicLike("%"+user.getHeadPic()+"%");
            }
            if (StringUtils.isNotBlank(user.getQq())) {
                criteria.andLike("qq", "%" + user.getQq() + "%");
                //criteria.andQqLike("%"+user.getQq()+"%");
            }
            if (StringUtils.isNotBlank(user.getIsMobileCheck())) {
                criteria.andLike("isMobileCheck", "%" + user.getIsMobileCheck() + "%");
                //criteria.andIsMobileCheckLike("%"+user.getIsMobileCheck()+"%");
            }
            if (StringUtils.isNotBlank(user.getIsEmailCheck())) {
                criteria.andLike("isEmailCheck", "%" + user.getIsEmailCheck() + "%");
                //criteria.andIsEmailCheckLike("%"+user.getIsEmailCheck()+"%");
            }
            if (StringUtils.isNotBlank(user.getSex())) {
                criteria.andLike("sex", "%" + user.getSex() + "%");
                //criteria.andSexLike("%"+user.getSex()+"%");
            }

        }
        List<TbUser> all = userMapper.selectByExample(example);
        PageInfo<TbUser> info = new PageInfo<TbUser>(all);
        //序列化再反序列化
        String s = JSON.toJSONString(info);
        PageInfo<TbUser> pageInfo = JSON.parseObject(s, PageInfo.class);

        return pageInfo;
    }

    @Override
    public void updateOne(Long id, String flag) {
        //1 查找到id对应的用户  根据用户的id的status进行更新数据库
        TbUser user = new TbUser();
        user.setId(id);
        user.setStatus(flag);
        userMapper.updateByPrimaryKeySelective(user);
    }

    @Override
    public void updateList(Long[] ids, String flag) {
        Example example = new Example(TbUser.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andIn("id", Arrays.asList(ids));
        //criteria.andEqualTo("status",flag);
        List<TbUser> tbUsers = userMapper.selectByExample(example);
        for (TbUser tbUser : tbUsers) {
            tbUser.setStatus(flag);
            userMapper.updateByPrimaryKeySelective(tbUser);
        }
    }

    @Override
    public List<Integer> findUserCountByMonths(List<String> monthList) {
        List<Integer> userCountList = new ArrayList<Integer>();

        //遍历
        for (String date : monthList) {
            //2019-06
            date = date + "-31";
            //查询 SELECT count(*) FROM t_member WHERE regTime <= '2019-06-31'
            Integer userCount = userMapper.findUserCountBeforeDate(date);

            userCountList.add(userCount);

        }
        return userCountList;
    }

    /**
     * 获取execl数据
     *
     * @return
     */
    @Override
    public List<Map<String, Object>> getBusinessReportData() {
        List<Map<String, Object>> maps = userMapper.getBusinessReportData();
        System.out.println("5555555555555");
        return maps;
    }

    /**
     * reportDate:null,    //日期
     * todayNewMember :0,  //今天新增会员数(统计今天的会员总数量)
     * totalMember :0,     //总会员数
     * thisWeekNewMember :0, //本周新增会员数
     * thisMonthNewMember :0, //本月新增会员数
     * todayOrderNumber :0,  //今日预约数
     * thisWeekOrderNumber :0, //本周预约数
     * thisMonthOrderNumber :0, //本月预约数
     *
     * @return
     */
    @Override
    public Map<String, Object> getBusinessReport() {

        try {
            //获得当前日期
//            String report= DateUtils.getToday().toLocaleString();
//            String reportDate = DateUtil.parseYYYYMMDDDate(report).toLocaleString();
//            //获得本周一的日期
//            String thisWeek = DateUtils.getThisWeekMonday().toLocaleString();
//            String thisWeekMonday = DateUtil.parseYYYYMMDDDate(thisWeek).toLocaleString();
//            //获得本月第一天的日期
//            String firstDay4This = DateUtils.getFirstDay4ThisMonth().toLocaleString();
//            String firstDay4ThisMonth = DateUtil.parseYYYYMMDDDate(firstDay4This).toLocaleString();
            String reportDate= DateUtils.parseDate2String(DateUtils.getToday());
            //获得本周一的日期
            String thisWeekMonday= DateUtils.parseDate2String(DateUtils.getThisWeekMonday());
            //获得本月第一天的日期
            String firstDay4ThisMonth= DateUtils.parseDate2String(DateUtils.getFirstDay4ThisMonth());
            //tbOrderMapper   userMapper
            //今日新增会员数
            Integer todayNewMember = userMapper.findUserCountByDate(reportDate);
            //总会员数
            Integer totalMember = userMapper.findUserTotalCount();
            //本周新增会员数
            Integer thisWeekNewMember = userMapper.findUserCountAfterDate(thisWeekMonday);
            //本月新增会员数
            Integer thisMonthNewMember = userMapper.findUserCountAfterDate(firstDay4ThisMonth);
            //今日预约数
            Integer todayOrderNumber = tbOrderMapper.findOrderCountByDate(reportDate);
            //本周预约数
            Integer thisWeekOrderNumber = tbOrderMapper.findOrderCountWeekDate(thisWeekMonday);
            //本月预约数
            Integer thisMonthOrderNumber = tbOrderMapper.findOrderCountAfterDate(firstDay4ThisMonth);
            Map<String,Object> result = new HashMap<>();
            result.put("reportDate",reportDate);
            result.put("todayNewMember",todayNewMember);
            result.put("totalMember",totalMember);
            result.put("thisWeekNewMember",thisWeekNewMember);
            result.put("thisMonthNewMember",thisMonthNewMember);
            result.put("todayOrderNumber",todayOrderNumber);
            result.put("thisWeekOrderNumber",thisWeekOrderNumber);
            result.put("thisMonthOrderNumber",thisMonthOrderNumber);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return new HashMap<>();
        }

    }

    public static void main(String[] args) {
        try {
            String today = DateUtils.parseDate2String(DateUtils.getToday());
            String s = DateUtil.parseYYYYMMDDDate(today).toLocaleString();

            System.out.println(s);
            System.out.println(today);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
