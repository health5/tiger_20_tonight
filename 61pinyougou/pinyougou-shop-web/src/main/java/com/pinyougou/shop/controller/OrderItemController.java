package com.pinyougou.shop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.pinyougou.order.service.OrderItemService;
import com.pinyougou.pojo.OrderCount;
import com.pinyougou.pojo.TbOrderItem;
import entity.OrderCountTerm;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * controller
 * @author Administrator
 *
 */
@RestController
@RequestMapping("/orderItem")
public class OrderItemController {

	@Reference
	private OrderItemService orderItemService;
	
	/**
	 * 返回全部列表
	 * @return
	 */
	@RequestMapping("/findAll")
	public List<TbOrderItem> findAll(){
		return orderItemService.findAll();
	}
	
	
	
	@RequestMapping("/findPage")
    public PageInfo<TbOrderItem> findPage(@RequestParam(value = "pageNo", defaultValue = "1", required = true) Integer pageNo,
                                          @RequestParam(value = "pageSize", defaultValue = "10", required = true) Integer pageSize) {
        return orderItemService.findPage(pageNo, pageSize);
    }
	

	
	/**
	 * 获取实体
	 * @param id
	 * @return
	 */
	@RequestMapping("/findOne/{id}")
	public TbOrderItem findOne(@PathVariable(value = "id") Long id){
		return orderItemService.findOne(id);		
	}



	@RequestMapping("/search")
    public PageInfo<TbOrderItem> findPage(@RequestParam(value = "pageNo", defaultValue = "1", required = true) Integer pageNo,
                                          @RequestParam(value = "pageSize", defaultValue = "10", required = true) Integer pageSize,
                                          @RequestBody TbOrderItem orderItem) {
        return orderItemService.findPage(pageNo, pageSize, orderItem);
    }

	@RequestMapping("/orderCount")
	public PageInfo<OrderCount> orderCount(@RequestParam(value = "pageNo", defaultValue = "1", required = true)Integer pageNo,
                                           @RequestParam(value = "pageSize", defaultValue = "10", required = true)Integer pageSize,
                                           @RequestBody OrderCountTerm orderCountTerm){
		String sellerId= SecurityContextHolder.getContext().getAuthentication().getName();
		orderCountTerm.setSellerId(sellerId);
		return  orderItemService.orderCount(pageNo,pageSize,orderCountTerm);

	}
	
}
